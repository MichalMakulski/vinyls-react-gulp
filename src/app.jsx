;(function(){
  
  var React = require('react');
  var Header = require('./header');
  var Jukebox = require('./jukebox');
  var gramophoneData = require('./data');

  var myHeader = React.createElement(Header);
  var myJukebox = React.createElement(Jukebox, gramophoneData);

  React.render(myHeader, document.querySelector('.header'));
  React.render(myJukebox, document.querySelector('.main'));
  
})();