var React = require('react');

module.exports = React.createClass({
  clickHandler: function(action) {
    if (action === 'play') {
      var trackSrc = 'assets/audio/' + this.props.trackSrc + '.mp3';
      this.props.playRecord(trackSrc);
    }
    
    if (action === 'pause') {
      this.props.pauseRecord();
    }
    
    this.props.updateState(action);
    
  },
  render: function() {
    return <div className="controls" >
        <i onClick={this.clickHandler.bind(this, 'play')} className="fa fa-play fa-lg" aria-hidden="true"></i>
        <i onClick={this.clickHandler.bind(this, 'pause')} className="fa fa-stop fa-lg" aria-hidden="true"></i>
     </div>
  }
  
});