module.exports = {
  vinyls: [
    {
      artist: 'Judy Collins',
      track: 'Someday soon',
      shortName: 'judy'
    },
    {
      artist: 'The Doors',
      track: 'Riders on the storm',
      shortName: 'doors'
    },
    {
      artist: 'Leonard Cohen',
      track: 'Suzanne',
      shortName: 'leonard'
    }
  ]
};