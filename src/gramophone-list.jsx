var React = require('react');
var Gramophone = require('./gramophone');

module.exports = React.createClass({
  render: function() {
    var list = this.props.vinyls.map(function(vinyl){
      return <Gramophone 
        className='gramophone'
        playRecord={this.props.playRecord} 
        pauseRecord={this.props.pauseRecord}
        recordOn={this.props.recordOn}
        {...vinyl} 
      />
    }.bind(this));

    return <div>
      {list}
    </div>
  }
});