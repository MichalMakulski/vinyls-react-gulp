var React = require('react');
var Plate = require('./plate');
var TrackInfo = require('./track-info');
var Controls = require('./controls');

module.exports = React.createClass({
  getInitialState: function() {
    return {
      recordOn: false
    }
  },
  updateState: function(action) {
    this.setState({
      recordOn: (action === 'play') ? true : false
    });
  },
  render: function() {
    var artistShortName = this.props.shortName;
    var vinylImageSrc = 'assets/images/' + artistShortName + '.png';
    
    return <div className={this.props.className + (this.state.recordOn ? ' record-on' : '')} >
        <Plate vinylImage={vinylImageSrc} />
        <TrackInfo track={this.props.track} artist={this.props.artist} />
        <Controls 
          trackSrc={artistShortName} 
          playRecord={this.props.playRecord} 
          pauseRecord={this.props.pauseRecord} 
          updateState={this.updateState}
        />
      </div>
  }
  
});