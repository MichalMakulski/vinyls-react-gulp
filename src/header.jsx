var React = require('react');

module.exports = React.createClass({
  
  render: function() {
    return <div className="header-inner">
        <h1>MY LPs</h1>
        <h6><i className="fa fa-headphones"></i> Put on your head-phones</h6>
        <h4><em>It ain't no joke when you lose your vinyl</em><br /><span>- Afrika Bambaataa</span></h4>
     </div>
  }
  
});