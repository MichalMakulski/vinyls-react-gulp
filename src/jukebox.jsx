var React = require('react');
var GramophoneList = require('./gramophone-list');
var audio = new Audio();

module.exports = React.createClass({
  render: function() {
    return <div className="jukebox">
        <GramophoneList 
          playRecord={this.playRecord} 
          pauseRecord={this.pauseRecord}
          vinyls={this.props.vinyls} 
        />

      </div>
  },
  playRecord: function(trackSrc) {
    audio.src = trackSrc;
    audio.load();
    setTimeout(function(){ audio.play(); }, 1200);
  },
  pauseRecord: function() {
    audio.pause();
  }
});