var React = require('react');

module.exports = React.createClass({
  
  render: function() {
    return <div className="plate">
              <img className="lp play" src={this.props.vinylImage} alt={this.props.artist} />
              <img className="arm" src="assets/images/arm2.png" alt="Phono arm"/>
           </div>
  }
  
});