var React = require('react');

module.exports = React.createClass({
  
  render: function() {
    return <div className="track-info">
        <span><em>{this.props.track}</em><br />{this.props.artist}</span>
     </div>
  }
  
});